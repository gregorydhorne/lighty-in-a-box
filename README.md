# Web Server in a Docker Container

Serve static website content using a light-weight web server.

## General Usage Information

lighttpd is a highly configurable light-weight web server. See the official
[documentation][1] for the Lighty project.

File: lighttpd.conf

```
server.document-root = "/var/www/servers/pages/"

server.port = 80

server.username = "lighttpd"
server.groupname = "lighttpd"

mimetype.assign = (
  ".css" => "text/css",
  ".html" => "text/html",
  ".txt" => "text/plain",
  ".jpg" => "image/jpeg",
  ".png" => "image/png"
)

static-file.exclude-extensions = ( ".fcgi", ".php", ".rb", "~", ".inc" )
index-file.names = ( "index.html" )

server.pid-file                 = "/var/run/lighttpd.pid"
dir-listing.encoding            = "utf-8"

$HTTP["host"] == "127.0.0.1" {
  server.document-root = "/var/www/servers/pages/"
}

```

To start the web server use the wrapper script.

```
$ ./lighttpd.sh

2019-12-14 02:44:39: (server.c.1521) server started (lighttpd/1.4.54)
```

To terminate the web server, press CTRL-C.

[1]: https://redmine.lighttpd.net/projects/lighttpd/wiki

FROM alpine:3.10

LABEL maintainer="greg@gregoryhorne.ca"

LABEL org.label-schema.schema-version="1.4.54-r0"
LABEL org.label-schema.build-date="2019-12-14T02:04:47Z"
LABEL org.label-schema.version="1.4.54"
LABEL org.label-schema.docker.cmd="docker run --rm --interactive --tty -p 80:80 --volume "${PWD}":/var/www/servers/pages --name lighttpd lighttpd"

RUN apk --no-cache update upgrade \
    && apk --no-cache add lighttpd \
    && mkdir /var/www/servers /var/www/servers/pages

COPY lighttpd.conf /etc/lighttpd/lighttpd.conf

EXPOSE 80
VOLUME /var/www/servers/pages

ENTRYPOINT ["sh", "-c", "lighttpd -D -f /etc/lighttpd/lighttpd.conf"]

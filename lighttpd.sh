#!/usr/bin/env sh

echo
docker run --rm --interactive --tty -p 80:80 --volume "${PWD}":/var/www/servers/pages --name lighttpd lighttpd:1.4.54
echo

exit 0
